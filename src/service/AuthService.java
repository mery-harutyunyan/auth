package service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Scanner;

public class AuthService {
    private static final UserService service = new UserService();

    public static void submitRegisterForm(Scanner scanner) {
        String fullName, userName, email, password;


        boolean isValid = false;
        do {
            System.out.println("Enter your full name");
            fullName = scanner.nextLine();
            if (fullName.matches("([A-Z]{1}[a-z]+)( )([A-Z]{1}[a-z]+)"))
                isValid = true;
            else
                System.out.println("Full name must contains two uppercase started string");
        } while (!isValid);

        isValid = false;
        do {
            System.out.println("Enter your username");
            userName = scanner.nextLine();
            if (userName.length() > 10 && !service.existUserName(userName))
                isValid = true;
            else
                System.out.println("Username must have at least 10 characters and be unique");
        } while (!isValid);

        isValid = false;
        do {
            System.out.println("Enter your email");
            email = scanner.nextLine();
            if (email.matches("^(.+)@(.+)$"))
                isValid = true;
            else
                System.out.println("Email must be valid");
        } while (!isValid);

        isValid = false;
        do {
            System.out.println("Enter your password");
            password = scanner.nextLine();
            if (password.matches("^(?=.{8,}$)(?=(?:.*?[A-Z]){2})(?=(?:.*?[0-9]){3}).*$"))
                isValid = true;
            else
                System.out.println("Password must contains 2 uppercase letters, 3 digits and has at least 8 character");
        } while (!isValid);

        register(fullName, userName, email, password);
    }

    public static void submitLoginForm(Scanner scanner) {
        String email, password;
        boolean isValid = false;
        do {
            System.out.println("Enter your email");
            email = scanner.nextLine();
            if (email.matches("^(.+)@(.+)$"))
                isValid = true;
            else
                System.out.println("Email must be valid");
        } while (!isValid);

        isValid = false;
        do {
            System.out.println("Enter your password");
            password = scanner.nextLine();
            if (password.matches("^(?=.{8,}$)(?=(?:.*?[A-Z]){2})(?=(?:.*?[0-9]){3}).*$"))
                isValid = true;
            else
                System.out.println("Password must contains 2 uppercase letters, 3 digits and has at least 8 character");
        } while (!isValid);

        login(email, password);
    }

    public static void register(String fullName, String userName, String email, String password) {
        service.saveUser(Arrays.asList(fullName, userName, email, md5(password)));
        System.out.println("User registered successfully!");
    }

    public static void login(String email, String password) {
        if (service.existUser(email, md5(password))) {
            System.out.println("Welcome to our system");
        } else {
            System.out.println("Wrong credentials");
        }
    }

    public static String md5(String input) {
        String md5 = null;
        if (null == input) return null;
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(input.getBytes(), 0, input.length());
            md5 = new BigInteger(1, digest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return md5;
    }
}