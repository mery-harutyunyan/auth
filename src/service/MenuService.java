package service;

import java.util.Scanner;

public class MenuService {
    public static final int REGISTER = 1;
    public static final int LOGIN = 2;
    public static final int EXIT = 3;

    public static void generate() {
        boolean isMenuActive = true;
        while (isMenuActive) {
            Scanner scanner = new Scanner(System.in);

            System.out.println("Please choose command number");
            System.out.println("1. Register");
            System.out.println("2. Login");
            System.out.println("3. Exit");

            int commandNumber = scanner.nextInt();
            scanner.nextLine();
            switch (commandNumber) {
                case REGISTER -> AuthService.submitRegisterForm(scanner);
                case LOGIN -> AuthService.submitLoginForm(scanner);
                case EXIT -> {
                    isMenuActive = false;
                    System.out.println("Bye");
                }
                default -> System.out.println("Wrong command number");
            }
        }
    }

}
