package service;

import model.User;

import java.util.ArrayList;
import java.util.List;

public class UserService {
    private final String path = "database.txt";
    private final ArrayList<User> users = generateUsers();

    public void saveUser(List<String> list) {
        FileService.write(path, String.join(",", list));
    }

    public boolean existUser(String email, String password) {
        for (User user : users)
            if (user.getEmail().equals(email) && user.getPassword().equals(password)) return true;

        return false;
    }

    public boolean existUserName(String userName) {
        for (User user : users)
            if (user.getUserName().equals(userName)) return true;

        return false;
    }

    public ArrayList<User> generateUsers() {
        ArrayList<String> usersList = FileService.read(path);

        ArrayList<User> users = new ArrayList<>();
        for (String user : usersList) {
            users.add(new User(user));
        }

        return users;
    }

}
