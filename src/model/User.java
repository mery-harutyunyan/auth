package model;

public class User {
    private String fullName;
    private String userName;
    private String email;
    private String password;

    public User(String user) {
        String[] d = user.split(",");
        this.fullName = d[0];
        this.userName = d[1];
        this.email = d[2];
        this.password = d[3];
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("User{")
                .append("fullName='")
                .append(fullName)
                .append('\'')
                .append(", userName='")
                .append(userName)
                .append('\'')
                .append(", email='")
                .append(email)
                .append('\'')
                .append(", password='")
                .append(password)
                .append('\'')
                .append('}')
                .toString();
    }
}
